#!/bin/sh
# publish local NPM core mirror
# nexus is used for NPM packages

# enable debugging
#set -x
LOGFILE=nodejs-publish.log
PUBDIR=/data1/repo/tpe-repo/buildtools/nodejs
if [ -z "$1" ]; then
	echo Missing channel version. Downloaded channels are:
	ls -1 data/
	exit
fi
VERSION=$1

if [ -d data/$VERSION ] && [ -d $PUBDIR ]; then
  # rotate PUBDIR if necessary
  if [ -d $PUBDIR/$VERSION ]; then 
    mv $PUBDIR/$VERSION $PUBDIR/$VERSION.`date +%F`
  fi
  mv data/$VERSION $PUBDIR/$VERSION
else
  if [ ! -d data/$VERSION ]; then
  echo "Local download \" data/$VERSION \" not found"
  fi
  if [ ! -d $PUBDIR ]; then
    echo "$PUBDIR not found"
  fi
fi
