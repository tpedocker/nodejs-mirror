#!/bin/sh
# create/update local NPM core mirror
# nexus is used for NPM packages
# wget manual: https://www.gnu.org/software/wget/manual/wget.html

# enable debugging
#set -x
LOGFILE=nodejs-mirror.log
SRCURL=https://nodejs.org/dist
PROXY=http://proxy.ontwikkel.local:8080

if [ ! -z "$PROXY" ]; then
  #PROXYOPTS=" -e http_proxy=$PROXY -e https_proxy=$PROXY"
  HTTP_PROXY=$PROXY
  HTTPS_PROXY=$PROXY
fi
env | grep -i PROXY

if [ -z "$1" ]; then
	echo missing channel version
	curl $SRCURL | grep latest
	exit
fi
VERSION=$1

VERSIONURL=$SRCURL/latest-v${VERSION}.x/

# robots.txt forbids downloads of other versions than latest
# we want to use latest LTS, so we need to ignore robots.txt
ROBOTSOFF="-e robots=off"

# strip extra directories
PATHOPTS="--no-directories --no-parent --recursive --level=1 -P data/${VERSION}"
# https certificate not trusted by wget, because RootCA of corporate proxy
#ACCEPT='-A "*.tmp","*.txt","*.zip","*.linux-x64.tar.gz","*.msi","*.pkg"'
ACCEPT="--accept html,txt,x64.msi,linux-x64.tar.gz,pkg,win-x64.zip"

#echo Phase 1: get file list
#time wget -o $LOGFILE $ROBOTSOFF $PATHOPTS $PROXYOPTS --continue $PROGRESS --wait 0.5 $REJECT $ACCEPT $SRCURL/index.html

PROGRESS="--progress=dot:mega --show-progress -v"
set -x
time wget -o $LOGFILE $ROBOTSOFF $PATHOPTS $PROXYOPTS --continue $PROGRESS --wait 0.5 $REJECT $ACCEPT $VERSIONURL
set +x
du -sh data/${VERSION}/*
